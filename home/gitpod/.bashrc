#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias grep='grep --color=auto'

parse_git_branch() {
    if [[ $(git branch 2> /dev/null) ]]; then
        if [[ $(git branch 2> /dev/null | sed -e 's/* //') != "arch-gitpod" ]]; then
            git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
        fi
    else
        return
    fi
}
PS1="\n\e[1;35m$(cat /etc/hostname | cut -d'-' -f1)\e[m\e[1;33m@\e[m\e[1;36mgitpod\e[m \e[1;32m\w\e[m\e[1;33m\$(parse_git_branch)\e[m\n\e[1;33m[ \t ]\e[m\n\n\[\e[1;32m\]$\[\e[m\] \[$(tput sgr0)\]"

sudo chown $USER $HOME
cd $HOME
